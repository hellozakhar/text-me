package com.hellozakhar.textme.service;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.hellozakhar.textme.model.User;
import com.hellozakhar.textme.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    UserService userService;

    @Mock
    private UserRepository userRepository;

    @Test
    public void testLoginUserWhenSuccess() {
        // Arrange
        Optional<User> user = Optional.of(new User("qwerty", BCrypt.withDefaults().hashToString(12, "123456".toCharArray())));
        boolean expected = true;

        when(userRepository.findByUsername("qwerty")).thenReturn(user);

        // Act
        boolean actual = userService.loginUser("qwerty", "123456");

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testLoginUserWhenWrongPassword() {
        // Arrange
        Optional<User> user = Optional.of(new User("qwerty", BCrypt.withDefaults().hashToString(12, "123456".toCharArray())));
        boolean expected = false;

        when(userRepository.findByUsername("qwerty")).thenReturn(user);

        // Act
        boolean actual = userService.loginUser("qwerty", "wrongPassword");

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testLoginUserWhenUserIsNotExists() {
        // Arrange
        Optional<User> user = Optional.empty();
        boolean expected = false;

        when(userRepository.findByUsername("qwerty")).thenReturn(user);

        // Act
        boolean actual = userService.loginUser("qwerty", "123456");

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testRegisterUserSuccess() {
        // Arrange
        User user = new User("qwerty", BCrypt.withDefaults().hashToString(12, "123456".toCharArray()));

        boolean expected = true;

        when(userRepository.existsByUsername("qwerty")).thenReturn(false);

        // Act
        boolean actual = userService.registerUser(user);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testRegisterUserWhenAlreadyExists() {
        // Arrange
        User user = new User("qwerty", BCrypt.withDefaults().hashToString(12, "123456".toCharArray()));

        boolean expected = false;

        when(userRepository.existsByUsername("qwerty")).thenReturn(true);

        // Act
        boolean actual = userService.registerUser(user);

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testFindAll() {
        // Arrange
        User userOne = new User("qwerty1", "someEncodedPassword1");
        User userTwo = new User("qwerty2", "someEncodedPassword2");
        List<User> expected = new ArrayList<>();
        expected.add(userOne);
        expected.add(userTwo);

        when(userRepository.findAll()).thenReturn(expected);
        // Act
        List<User> actual = userService.findAll();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testFindByUsernameWhenExists() {
        // Arrange
        Optional<User> expected = Optional.of(new User("qwerty", "someEncodedPassword"));

        when(userRepository.findByUsername("qwerty")).thenReturn(expected);
        // Act
        Optional<User> actual = userService.findByUsername("qwerty");

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testFindByUsernameWhenNotExists() {
        // Arrange
        Optional<User> expected = Optional.empty();

        // Act
        Optional<User> actual = userService.findByUsername("qwerty");

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testFindByIdWhenExists() {
        // Arrange
        Optional<User> expected = Optional.of(new User("qwerty", "someEncodedPassword"));
        expected.get().setId("0001");

        when(userRepository.findById(expected.get().getId())).thenReturn(expected);
        // Act
        Optional<User> actual = userService.findById(expected.get().getId());

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testFindByIdWhenNotExists() {
        // Arrange
        Optional<User> expected = Optional.empty();

        // Act
        Optional<User> actual = userService.findById("143214321432");

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetUserByUsernameWhenExists() {
        // Arrange
        Optional<User> expected = Optional.of(new User("qwerty", "someEncodedPassword"));
        expected.get().setId("0001");

        when(userService.findByUsername("qwerty")).thenReturn(expected);

        // Act
        Optional<User> actual = userService.getUserByUsername("qwerty");

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetUserByUsernameWhenNotExists() {
        // Arrange
        Optional<User> expected = Optional.empty();

        // Act
        Optional<User> actual = userService.getUserByUsername("qwerty");

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetUserByUserIdWhenExists() {
        // Arrange
        Optional<User> expected = Optional.of(new User("qwerty", "someEncodedPassword"));
        expected.get().setId("0001");

        when(userService.findById("0001")).thenReturn(expected);

        // Act
        Optional<User> actual = userService.getUserByUserId("0001");

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetUserByUserIdWhenNotExists() {
        // Arrange
        Optional<User> expected = Optional.empty();

        when(userService.findById("0001")).thenReturn(expected);

        // Act
        Optional<User> actual = userService.getUserByUserId("0001");

        // Assert
        assertEquals(expected, actual);
    }
}