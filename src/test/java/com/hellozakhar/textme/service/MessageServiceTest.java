package com.hellozakhar.textme.service;

import com.hellozakhar.textme.model.Message;
import com.hellozakhar.textme.repository.MessageRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MessageServiceTest {

    @InjectMocks
    MessageService messageService;       // create bean of MessageService for tests

    @Mock
    MessageRepository messageRepository; // create mock object of MessageRepository. It emulates MessageRepository's behaviour

    @Test
    public void testTemplate() {  // use this triple-A test pattern for all your tests
        // Arrange: prepare all data for tests

        // Act: call the method which you want to test

        // Assert: check actual and expected result
    }

    @Test
    public void testFindChatMessages() {
        // Arrange
        // create test message
        Message message = Message.builder().id("1234").build();

        // define expected result
        List<Message> expected = Collections.singletonList(message);

        // mock findByChatId method
        when(messageRepository.findByChatId("1234")).thenReturn(expected);

        // Act
        List<Message> actual = messageService.findChatMessages("1234");

        // Assert
        assertEquals(expected, actual);  // Notice: expected result always goes first
    }
}