package com.hellozakhar.textme.service;

import com.hellozakhar.textme.model.Participant;
import com.hellozakhar.textme.repository.ParticipantRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ParticipantServiceTest {

    @InjectMocks
    ParticipantService participantService;

    @Mock
    ParticipantRepository repository;

    @Test
    public void testFindUserChatsIfExists() {
        // Arrange
        // creating user as participant of chats "1000", "1020"
        Participant userFirstChat = Participant.builder().userId("0001").chatId("1000").build();
        Participant userSecondChat = Participant.builder().userId("0001").chatId("1020").build();
        List<Participant> participant = new ArrayList<>();
        participant.add(userFirstChat);
        participant.add(userSecondChat);

        List<String> expected = new ArrayList<>();
        expected.add("1000");
        expected.add("1020");

        when(repository.findByUserId("0001")).thenReturn(participant);

        // Act
        List<String> actual = participantService.findChatsByParticipantId("0001");

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testFindUserChatsIfNotExists() {
        // Arrange
        List<String> expected = new ArrayList<>();

        when(repository.findByUserId("0001")).thenReturn(new ArrayList<>());

        // Act
        List<String> actual = participantService.findChatsByParticipantId("0001");

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testFindChatUsersIfExists() {
        // Arrange
        // creating users "0001", "0002" as participants of chats "1000"
        Participant firstUserInChat = Participant.builder().userId("0001").chatId("1000").build();
        Participant secondUserInChat = Participant.builder().userId("0002").chatId("1000").build();
        List<Participant> participants = new ArrayList<>();
        participants.add(firstUserInChat);
        participants.add(secondUserInChat);

        List<String> expected = new ArrayList<>();
        expected.add("0001");
        expected.add("0002");

        when(repository.findByChatId("1000")).thenReturn(participants);

        // Act
        List<String> actual = participantService.findParticipantsByChatId("1000");

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testFindChatUsersIfNotExists() {
        // Arrange
        List<String> expected = new ArrayList<>();

        when(repository.findByChatId("1000")).thenReturn(new ArrayList<>());

        // Act
        List<String> actual = participantService.findParticipantsByChatId("1000");

        // Assert
        assertEquals(expected, actual);
    }
}