package com.hellozakhar.textme.service;

import com.hellozakhar.textme.model.Chat;
import com.hellozakhar.textme.model.Participant;
import com.hellozakhar.textme.repository.ChatRepository;
import com.hellozakhar.textme.repository.ParticipantRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChatServiceTest {

    @InjectMocks
    ChatService chatService;

    @Mock
    ChatRepository chatRepository;

    @Mock
    ParticipantRepository participantRepository;

    @Test
    public void testGetChatIdSuccess() {
        // Arrange
        // create participants of chat
        Participant firstParticipant = Participant.builder().userId("0001").chatId("1000").build();
        Participant secondParticipant = Participant.builder().userId("0002").chatId("1000").build();

        // create chat
        Optional<Chat> expected = Optional.ofNullable(Chat.builder().id("1000").build());

        // create lists of each user chats
        List<Participant> firstUserChats = Collections.singletonList(firstParticipant);
        List<Participant> secondUserChats = Collections.singletonList(secondParticipant);

        when(participantRepository.findByUserId("0001")).thenReturn(firstUserChats);
        when(participantRepository.findByUserId("0002")).thenReturn(secondUserChats);
        when(participantRepository.findByChatId("1000")).thenReturn(Arrays.asList(firstParticipant, secondParticipant));
        when(chatRepository.findById("1000")).thenReturn(expected);

        // Act
        Optional<Chat> actual = chatService.getChatId(Arrays.asList("0001", "0002"));

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetChatIdFail() {
        // Arrange
        // create participants of chat
        Participant firstParticipant = Participant.builder().userId("0001").chatId("1001").build();
        Participant secondParticipant = Participant.builder().userId("0002").chatId("1000").build();

        // create chat
        Optional<Chat> expected = Optional.empty();

        // create lists of each user chats
        List<Participant> firstUserChats = Collections.singletonList(firstParticipant);
        List<Participant> secondUserChats = Collections.singletonList(secondParticipant);

        when(participantRepository.findByUserId("0001")).thenReturn(firstUserChats);
        when(participantRepository.findByUserId("0002")).thenReturn(secondUserChats);

        // Act
        Optional<Chat> actual = chatService.getChatId(Arrays.asList("0001", "0002"));

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testCreateChat() {
        // Arrange
        // null because id is generating in mongo
        String expected = null;

        // Act
        String actual = chatService.createChat(Arrays.asList("0001", "0002"));

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void testGetAllChats() {
        // Arrange
        Chat chat = Chat.builder().id("1234").build();

        // define expected result
        List<Chat> expected = Collections.singletonList(chat);

        when(chatRepository.findAll()).thenReturn(expected);

        // Act
        List<Chat> actual = chatService.getAllChats();

        // Assert
        assertEquals(expected, actual);
    }
}