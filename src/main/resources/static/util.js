var URL = window.location.href.indexOf("localhost") > -1 ?
    'http://localhost:8080' : 'http://textme-env.eba-a8896ru7.eu-west-2.elasticbeanstalk.com';
const HTTP_CODE_UNAUTHORIZED = 401;
var notification = document.querySelector('.notification');

window.onload = function () {
    if (window.location.href === URL + '/signIn' || window.location.href === URL + '/signUp') {
        document.getElementById('usernameError').style.display = 'none';
        document.getElementById('passwordError').style.display = 'none';
    }
}

function signUp() {
    disableButtonTemporarily("signUpButton");
    removeCookie();

    let usernameValue = document.getElementById("usernameField").value;
    let passwordValue = document.getElementById("passwordField").value;

    if (!isValidCredentials(usernameValue, passwordValue))
        return;

    request({
        url: URL + "/signUp",
        method: "POST",
        body: JSON.stringify({'username': usernameValue, 'password': passwordValue}),
    })
        .then((response) => {
            if (response.ok) {
                createNotification("Successful signUp", "Thank you! You're successfully registered. Please login to continue!", 'success', notification);
                document.location.href = URL + "/signIn";
            } else if (response.status === HTTP_CODE_UNAUTHORIZED) {
                createNotification("Username already taken", "User with this username already exists. Please change username and try again!", 'danger', notification);
            }
        });
}

function signIn() {
    disableButtonTemporarily("signInButton");
    removeCookie();

    let usernameValue = document.getElementById("usernameField").value;
    let passwordValue = document.getElementById("passwordField").value;

    if (!isValidCredentials(usernameValue, passwordValue))
        return;

    request({
        url: URL + "/signIn",
        method: "POST",
        body: JSON.stringify({'username': usernameValue, 'password': passwordValue}),
    })
        .then((response) => {
            if (response.ok) {
                let userId = getUserIdFromMongo(usernameValue);
                setCookie(usernameValue, userId);
                document.location.href = URL + "/";
            } else if (response.status === HTTP_CODE_UNAUTHORIZED) {
                createNotification("Wrong Credentials", "Username or Password is incorrect. Please try again!", 'danger', notification);
            }
        });
}

/*
 * Checks input credentials(username and password).
 * Return true, if credentials are correct.
 * Otherwise, returns false
 */
function isValidCredentials(username, password = undefined) {
    if (password !== undefined) {
        document.getElementById('usernameError').style.display = 'none';
        document.getElementById('passwordError').style.display = 'none';
        document.getElementById("usernameField").blur();
        document.getElementById('usernameField').style.borderColor = '';
        document.getElementById("passwordField").blur();
        document.getElementById('passwordField').style.borderColor = '';
    }
    let usernameRegex = "^[a-zA-Z0-9]+([._]?[a-zA-Z0-9]+)*$";
    let passwordRegex = usernameRegex;
    let isUsernameError = false;
    let isPasswordError = false;

    if (username !== undefined) {
        if (username.length < 3 || username.length > 15) {
            if (password !== undefined) {
                document.getElementById('usernameField').focus();
                document.getElementById('usernameField').style.borderColor = 'red';
                document.getElementById('usernameError').style.display = '';
                document.getElementById('usernameError').innerHTML = "Username must contain 3-15 characters.";
            }
            isUsernameError = true;
        }

        if (!username.match(usernameRegex)) {
            if (password !== undefined && !isUsernameError) {
                document.getElementById('usernameField').focus();
                document.getElementById('usernameField').style.borderColor = 'red';
                document.getElementById('usernameError').style.display = '';
                document.getElementById('usernameError').innerHTML = "Username must contain only letters and numbers.";
            }
            isUsernameError = true;
        }
    }

    if (password !== undefined) {
        if (password.length < 6 || password.length > 20) {
            if (password !== undefined) {
                if (document.activeElement['id'] !== 'usernameField') {
                    document.getElementById('passwordField').focus();
                }
                document.getElementById('passwordField').style.borderColor = 'red';
                document.getElementById('passwordError').style.display = '';
                document.getElementById('passwordError').innerHTML = "Password must contain 6-20 characters.";
            }
            isPasswordError = true;
        }

        if (!password.match(passwordRegex)) {
            if (password !== undefined && !isPasswordError) {
                if (document.activeElement['id'] !== 'usernameField') {
                    document.getElementById('passwordField').focus();
                }
                document.getElementById('passwordField').style.borderColor = 'red';
                document.getElementById('passwordError').style.display = '';
                document.getElementById('passwordError').innerHTML = "Password must contain only letters and numbers.";
            }
            isPasswordError = true;
        }
    }

    return !(isUsernameError || isPasswordError);
}

/**
 * Get cookie with key 'name'
 */
function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));

    return matches ? decodeURIComponent(matches[1]) : undefined;
}

/**
 * Creates two cookies: first with username,
 * second with userid
 */
function setCookie(username, userid) {
    document.cookie = "username=" + username + "; ";
    document.cookie = "userid=" + userid + "; ";
}

function removeCookie() {
    document.cookie = "username=; Max-Age=0";
    document.cookie = "userid=; Max-Age=0";
}

/**
 * Get username from cookie.
 * Note: use it if user exists
 */
function getUsernameFromCookie() {
    return getCookie("username");
}

/**
 * Get userId from cookie.
 * Note: use it if user exists
 */
function getUserIdFromCookie() {
    return getCookie("userid");
}

/**
 * Returns userId by username from MongoDB, using get-request.
 * If we want get id of current user better use getUserIdFromCookie()
 */
function getUserIdFromMongo(username) {
    if (username === undefined) {
        return undefined;
    }

    let user = getResultOfSimpleGetRequest("/user?username=" + username);

    return user === 'null' ? undefined : JSON.parse(user).id;
}

/**
 * Sends a GET-request and returns response text.
 * @param localPath
 * @returns {string|undefined}
 */
function getResultOfSimpleGetRequest(localPath) {
    let xhr = new XMLHttpRequest();

    xhr.open('GET', URL + localPath, false);
    xhr.send();

    if (xhr.status != 200) {
        console.log(xhr.status + ': ' + xhr.statusText);
        return undefined;
    } else {
        return xhr.responseText;
    }
}

/**
 * Returns list of chats of current user from MongoDB, using get-request.
 */
function getUserChats() {
    var text = getResultOfSimpleGetRequest("/chat/user/" + getUserIdFromCookie());
    var userChats = [];

    // if text != "[]"
    if (text.length !== 2) {
        userChats = JSON.parse(text);
    }

    return userChats;
}

/**
 * Returns list of all chats from MongoDB, using get-request.
 */
function getAllChats() {
    if (getUsernameFromCookie() !== 'admin')
        return;

    var text = getResultOfSimpleGetRequest("/chat/all/");
    var chats = [];

    // if text != "[]"
    if (text.length !== 2) {
        text = JSON.parse(text);
        text.forEach(chat => chats.push(chat.id));
    }

    return chats;
}

/**
 * Get a username from MongoDB, using get-request.
 * @param id A user id.
 */
function getUsernameFromMongo(id) {
    var user = getResultOfSimpleGetRequest("/user/" + id);

    return user === 'null' ? undefined : JSON.parse(user).username;
}

/**
 * Get participants of chat from MongoDB, using get-request.
 * @param chatId A chat id.
 */
function getParticipantsByChatId(chatId) {
    let destinationUrl = "/chat/" + chatId;
    var text = getResultOfSimpleGetRequest(destinationUrl);
    var participants = [];

    // if text != "[]"
    if (text !== undefined) {
        if (text.length !== 2) {
            participants = JSON.parse(text);
        }
    }

    return participants;
}

/**
 * Returns list of messages of selected chat from MongoDB, using get-request.
 */
function getChatMessagesFromMongo(chatId) {
    var text = getResultOfSimpleGetRequest("/message?chatId=" + chatId);
    var chatMessages = [];

    // if text != "[]"
    if (text.length !== 2) {
        chatMessages = JSON.parse(text);
    }

    return chatMessages;
}

function findChatWithUser(userId) {
    if (userId === undefined) {
        return undefined;
    }

    var participants = [getUserIdFromCookie()].concat(userId).sort();
    let text = getResultOfSimpleGetRequest("/chat?userId=" + participants.join(','));

    // if chat not exists => create chat
    return text === 'null' ? undefined : JSON.parse(text).id;
}

/**
 * Prevents multiply clicks on button.
 * @param nameOfButton A button id.
 */
function disableButtonTemporarily(nameOfButton) {
    document.getElementById(nameOfButton).disabled = true;
    setTimeout(() => {
        document.getElementById(nameOfButton).disabled = false;
    }, 1000);
}

async function request(options) {
    const headers = new Headers();

    if (options.setContentType !== false) {
        headers.append("Content-Type", "application/json");
    }

    const defaults = {headers: headers};
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options);
}

// function called when the button to dismiss the message is clicked
function dismissMessage(notification) {
    // remove the .received class from the .notification widget
    notification.classList.remove('received');
}

function createNotification(messageTitle, messageText, messageType, notification, timeout = 4000) {
    if (notification === null)
        notification = document.querySelector('.notification');
    // messageType: 'info', 'success', 'warning', 'danger',

    const message = document.querySelector('.notification__message');
    message.querySelector('h1').textContent = messageTitle;
    message.querySelector('p').textContent = messageText;
    message.className = `notification__message message--${messageType}`;

    // add a class of .received to the .notification container
    notification.classList.add('received');

    // attach an event listener on the button to dismiss the message
    // include the once flag to have the button register the click only one time
    const button = document.querySelector('.notification__message button');
    button.addEventListener('click', dismissMessage, {once: true});

    if (timeout > 0) {
        setTimeout(() => {
            dismissMessage(notification);
        }, timeout);
    }
}