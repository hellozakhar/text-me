var URL = window.location.href.indexOf("localhost") > -1 ?
    'http://localhost:8080' : 'http://textme-env.eba-a8896ru7.eu-west-2.elasticbeanstalk.com';
var chatIdList = [];
var chatTitleList = [];
var chatMessagesList = [];
var userIdList = [];
var userUsernameList = [];
var selectedChatId;
var selectedChatTitle;
var stompClient = null;
var reconnected = false;

var isCreatingGroupChatMode = false;
var groupChatRecipientsList = [];
var selectedChatsList = [];

var notification = document.querySelector('.notification');

UPLOADCARE_PUBLIC_KEY = 'ef34797e19703a4a68f6';
UPLOADCARE_LOCALE = 'ru';

const isAdmin = getUsernameFromCookie() === "admin";
var selectedMessagesIdList = [];

/**
 * Auto-load function which checks user logged or not
 * If not => redirect to homepage
 */
function initIndex() {
    // get username from cookie
    const username = getUsernameFromCookie();

    // check username exists in mongodb or not
    // NOTE: maybe user has an old cookie
    const userIdFromMongo = getUserIdFromMongo(username);

    if (userIdFromMongo === undefined || getUserIdFromCookie() !== userIdFromMongo) {
        window.stop();
        document.location.href = URL + "/signIn";
    } else {
        if (reconnected) {
            drawChatList();
            connect();
            reconnected = false;
        }
    }
}

// runs initIndex() before page loads
initIndex();

$(document).ready(function () {

    $('#headTitle').hide();
    $(".right-section").hide();

    drawChatList();
    connect();

    $('.main-section').fadeIn(500);

    $('#sendMessageButton').click(function () {
        // temporarily disable button of send message
        $('#sendMessageButton').prop('disabled', true);
        setTimeout(() => {
            $('#sendMessageButton').prop('disabled', false);
        }, 700);

        const messageField = getMessageFieldValue();

        // if message doesn't contain any symbols -> do nothing
        if (!messageField.trim().length)
            return;

        sendMessage();
        clearMessageField();
    });

    $('#searchButton').click(function () {
        disableButtonTemporarily("searchButton");

        const searchField = getSearchFieldValue();
        var userId;

        $('#searchField').css({
            'width': '73%',
            'border-radius': '10px',
            'border': '1px solid #E6E6E6',
            'padding': '7px',
        });

        $('#searchButton').hide();

        if (!isValidCredentials(searchField) || searchField === getUsernameFromCookie()) {
            createNotification("Incorrect username", "Login must be between 3 to 15 characters, contain only letters and numbers", 'warning', notification);
            return;
        }

        const pos = chatTitleList.indexOf(searchField);

        if (pos > -1) {
            console.log("search: chatId is " + chatIdList[pos] + ".");
            // case when chat already in chatList

            // if selected => do nothing
            if (chatIdList[pos] === selectedChatId) {
                console.log("search: chat already selected.");
                clearSearchField();
                return;
            }

            console.log("search: selecting chat.");
            setTimeout(function () {
                $("#listOfChats").find("#" + chatIdList[pos]).trigger('click');
            }, 200);
        } else {
            userId = getUserIdFromMongo(searchField);

            if (userId === undefined) {
                // case when user not exist
                createNotification("User has not been found", "User with username '" + searchField + "' does not exist. Enter another username and try again", 'warning', notification);
            } else {
                // get chatId with this user
                var chatId = findChatWithUser(userId);

                if (chatId === undefined) {
                    var participants = [getUserIdFromCookie()].concat(userId).sort();
                    request(
                        {
                            url: URL + "/chat/create",
                            method: "POST",
                            body: JSON.stringify(participants),
                        })
                        .then((response) => {
                            return response.text();
                        })
                        .then((result) => {
                            chatId = result;
                            const recipients = [userId];
                            const recipientsAndAdmin = recipients.concat(getUserId('admin'));

                            for (var i = 0; i < recipientsAndAdmin.length; i += 1) {
                                stompClient.subscribe('/topic-chat/' + recipientsAndAdmin[i], function (msg) {
                                }, {id: 'send-create-chat-with-user-' + recipientsAndAdmin[i]});
                            }

                            stompClient.send("/app/topic-chat", {}, JSON.stringify({id: chatId,}));

                            for (var i = 0; i < recipientsAndAdmin.length; i += 1) {
                                stompClient.unsubscribe('send-create-chat-with-user-' + recipientsAndAdmin[i]);
                            }

                            const chatTitle = getUsername(userId);

                            if (chatIdList.indexOf(chatId) > -1) {
                                console.log("search: chatId is " + chatId + ".");

                                // if selected => do nothing
                                if (chatId === selectedChatId) {
                                    console.log("search: chat already selected.");
                                    clearSearchField();
                                    return;
                                }

                                console.log("search: selecting chat.");
                                setTimeout(function () {
                                    $("#listOfChats").find("#" + chatId).trigger('click');
                                }, 200);
                            } else {
                                console.log("search: chat is not exist. Creating...");
                                chatIdList.push(chatId);
                                chatTitleList.push(chatTitle);
                                chatMessagesList[chatIdList.indexOf(chatId)] = [];

                                var chatLastMessage = getLastMessageContent(chatId)
                                var chatLastMessageTime = '';

                                stompClient.subscribe('/chat/messages/' + chatId,
                                    function (msg) {
                                        onMessageReceived(msg);
                                    }, {id: 'chat-' + chatId}
                                );

                                console.log("search: created chat " + chatId + ".");
                                createChatInChatList(chatId, chatTitle, chatLastMessageTime, chatLastMessage);
                            }
                        });
                }
            }
        }

        clearSearchField();
    });


    $('#searchField').on('keyup', function () {
        var $this = $(this),
            val = $this.val();

        if (val.length >= 1) {
            $('#searchButton').show();
            $('#searchField').css({
                'width': '60%',
                'border-radius': '10px',
                'border': '1px solid #E6E6E6',
                'padding': '7px',
            });
        } else {
            $('#searchButton').hide();
            $('#searchField').css({
                'width': '73%',
                'border-radius': '10px',
                'border': '1px solid #E6E6E6',
                'padding': '7px',
            });
        }
    });

    $('#exitButton').click(function () {
        disableButtonTemporarily("exitButton");
        clearSearchField();
        clearMessageField();
        $("body").fadeOut(500);
        disconnect();
        removeCookie();
        document.location.href = URL + "/home";
    });

    $('#createGroupChatButton').click(function () {
        if (chatIdList.length < 2) {
            // there's no choice in chat list
            createNotification("Can't create group chat", "A group chat cannot be created because the number of known contacts is less than two. Create at least two chats and try again.", 'warning', notification);
            return;
        }

        if (isCreatingGroupChatMode) {
            if (selectedChatsList.length < 2) {
                disableCreatingGroupChatMode();
                return;
            } else {
                for (var i = 0; i < selectedChatsList.length; i += 1) {
                    groupChatRecipientsList.concat(getParticipantsByChatId(selectedChatsList[i]))
                        .forEach(item => {
                            if (groupChatRecipientsList.indexOf(item) === -1) {
                                if (item !== getUserIdFromCookie()) {
                                    groupChatRecipientsList.push(item);
                                }
                            }
                        });
                }

                // find chat
                var chatId = findChatWithUser(groupChatRecipientsList);

                // if chat not exists then create
                if (chatId === undefined) {
                    groupChatRecipientsList = groupChatRecipientsList.sort();

                    request(
                        {
                            url: URL + "/chat/create",
                            method: "POST",
                            body: JSON.stringify([getUserIdFromCookie()].concat(groupChatRecipientsList).sort()),
                        })
                        .then((response) => {
                            return response.text();
                        })
                        .then((result) => {
                            chatId = result;
                            chatIdList.push(chatId);
                            var groupChatRecipientsListAndAdmin = groupChatRecipientsList.concat(getUserId('admin'));

                            for (var i = 0; i < groupChatRecipientsListAndAdmin.length; i += 1) {
                                stompClient.subscribe('/topic-chat/' + groupChatRecipientsListAndAdmin[i], function (msg) {
                                });
                            }

                            stompClient.send("/app/topic-chat", {}, JSON.stringify({id: chatId,}));

                            for (var i = 0; i < groupChatRecipientsListAndAdmin.length; i += 1) {
                                stompClient.unsubscribe('/topic-chat/' + groupChatRecipientsListAndAdmin[i]);
                            }

                            var chatRecipientsUsername = [];
                            groupChatRecipientsList.forEach(id => chatRecipientsUsername.push(getUsername(id)));
                            var chatTitle = chatRecipientsUsername.join(', ');
                            chatTitleList.push(chatTitle);
                            var lastMessage = getLastMessageContent(chatId);
                            var lastMessageTime = getLastMessageTime(chatId);

                            disableCreatingGroupChatMode();

                            stompClient.subscribe('/chat/messages/' + chatId,
                                function (msg) {
                                    onMessageReceived(msg);
                                }, {id: 'chat-' + chatId}
                            );

                            createChatInChatList(chatId, chatTitle, lastMessageTime, lastMessage);
                            setTimeout(function () {
                                $("#listOfChats").find("#" + chatId).trigger('click');
                            }, 200);

                            return;
                        });
                } else {
                    setTimeout(function () {
                        $("#listOfChats").find("#" + chatId).trigger('click');
                    }, 200);
                    disableCreatingGroupChatMode();
                    return;
                }
            }
        } else {
            isCreatingGroupChatMode = true;

            // hiding buttons
            $('#exitButton').hide();
            $('#searchField').hide();
            $('#searchButton').hide();

            // show 'create chat' text
            $('#headTitle').show();

            createNotification("Create group chat", "To create a group chat, you must select at least two known chat rooms and click the create group chat button", 'info', notification);
        }

        clearSearchField();
        makeChatNonActive(selectedChatId);
    });

    $(document).on('click', '#listOfChats li', function () {
        if (!isCreatingGroupChatMode) {
            if (selectedChatId === $(this).attr('id'))
                return;

            console.log("li-click: clicked on chat (" + $(this).attr('title') + ") with id " + $(this).attr('id') + ".");
            makeChatActive($(this).attr('id'), $(this).attr('title'));

            var chatMessageList = getChatMessages(selectedChatId);

            if (chatMessageList.length === 0)
                return;

            for (var i = 0; i < chatMessageList.length; i += 1) {
                var currentMessage = chatMessageList[i];
                var messageTime = new Date(currentMessage.timeCreated).toLocaleTimeString('en-US', {
                    hour12: false,
                    hour: "numeric",
                    minute: "numeric"
                });

                createMessageElement(currentMessage);

                if (i === chatMessageList.length - 1) {
                    updateChatHeaderRight(selectedChatTitle, currentMessage.content);
                    updateChatInChatList(selectedChatId, currentMessage.content, messageTime);
                }
            }
        } else {
            // if we selecting chats to create group chat
            // (isSelectingGroupChatMode = true)

            var currentChat = $(this).attr('id');

            // if chat already selected (chat in selectedChatsList)
            if (selectedChatsList.indexOf(currentChat) > -1) {
                selectedChatsList = selectedChatsList.filter(chatId => chatId !== currentChat);
                makeChatNonActive(currentChat);
            } else {
                selectedChatsList.push(currentChat);
                makeChatActive(currentChat);
            }
        }
    });

    $(document).on('click', '#listOfCurrentChatMessages li', function () {
        if (!isAdmin)
            return;

        const messageId = $(this).attr('id');
        console.log("message-click: clicked on message: " + messageId);

        // if message already selected (message in selectedMessagesIdList)
        if (selectedMessagesIdList.indexOf(messageId) > -1) {
            makeMessageNonActive(messageId)
        } else {
            makeMessageActive(messageId);
            createNotification("Deleting messages", "To delete messages, click on the trash can icon to the left of the message entry field.", 'info', notification);
        }
    });

    $('#clearMessagesButton').click(function () {
        disableButtonTemporarily("clearMessagesButton");

        if (!isAdmin || selectedMessagesIdList.length === 0)
            return;

        console.log("clear-messages: pressed button.");

        var selectedMessages = [];
        var chatMessages = getChatMessages(selectedChatId);
        for (var i = 0; i < selectedMessagesIdList.length; i += 1) {
            for (var j = 0; j < chatMessages.length; j += 1) {
                if (selectedMessagesIdList[i] === chatMessages[j].id) {
                    selectedMessages.push(chatMessages[j]);
                    break;
                }
            }
        }

        request(
            {
                url: URL + "/message/delete",
                method: "POST",
                body: JSON.stringify(selectedMessages),
            })
            .then((response) => {
                return response.text();
            })
            .then((result) => {
                console.log("message-delete: selectedMessages = " + selectedMessages);
                var adminId = getUserId('admin');
                var groupChatRecipientsList = getParticipantsByChatId(selectedChatId);
                var groupChatRecipientsListAndAdmin;
                if (groupChatRecipientsList.indexOf(adminId) > -1) {
                    groupChatRecipientsListAndAdmin = groupChatRecipientsList;
                } else {
                    groupChatRecipientsListAndAdmin = groupChatRecipientsList.concat(getUserId('admin'));
                }

                for (var i = 0; i < groupChatRecipientsListAndAdmin.length; i += 1) {
                    stompClient.subscribe('/topic-message/' + groupChatRecipientsListAndAdmin[i], function (msg) {
                    }, {id: 'delete-message-in-user-' + groupChatRecipientsListAndAdmin[i]});
                }

                stompClient.send("/app/topic-message", {}, JSON.stringify(selectedMessages));

                for (var i = 0; i < groupChatRecipientsListAndAdmin.length; i += 1) {
                    stompClient.unsubscribe('delete-message-in-user-' + groupChatRecipientsListAndAdmin[i]);
                }
            });

        $('#clearMessagesButton').hide();
    });

    $('#uploadImageButton').click(function () {
        $("#image-file").trigger('click');
    });

    $(window).on("unload", function () {
        disconnect();
    });
});

$(document).keyup(function (e) {
    if (e.key === "Escape") {
        const searchField = $('#searchField');

        makeChatNonActive(selectedChatId);
        searchField.blur();
        clearSearchField();
        $('#searchButton').hide();
        searchField.css({
            'width': '73%',
            'border-radius': '10px',
            'border': '1px solid #E6E6E6',
            'padding': '7px',
        });

        if (isCreatingGroupChatMode) {
            for (var i = 0; i < selectedChatsList.length; i += 1) {
                makeChatNonActive(selectedChatsList[i]);
            }

            disableCreatingGroupChatMode();
        }
    } else if (e.key === "Enter") {
        if ($("#sendMessageField").is(":focus")) {
            e.preventDefault();

            if (!$("#sendMessageButton").is(":disabled")) {
                setTimeout(function () {
                    $("#sendMessageButton").trigger('click');
                }, 50);
            }
        }

        if ($("#searchField").is(":focus")) {
            e.preventDefault();

            if (!$("#searchButton").is(":disabled")) {
                setTimeout(function () {
                    $("#searchButton").trigger('click');
                }, 50);
            }
        }
    }
});

function drawChatList() {
    makeChatNonActive();

    chatIdList = getUsernameFromCookie() === 'admin' ? getAllChats() : getUserChats();
    chatMessagesList = [];

    if (chatIdList.length === 0) {
        $('#listOfChats').empty();
        return;
    }

    var chatId;
    var chatTitle;
    var chatRecipientsId;
    var chatRecipientsUsername;
    var chatMessages;
    var chatLastMessage = '';
    var chatLastMessageTime = '';

    for (var i = 0; i < chatIdList.length; i += 1) {
        chatId = chatIdList[i];
        chatMessages = getChatMessages(chatId);
        chatLastMessage = getLastMessageContent(chatId);
        chatLastMessageTime = getLastMessageTime(chatId);

        if ($('#listOfChats').find('#' + chatId).length === 0) {
            chatRecipientsId = getParticipantsByChatId(chatId).filter(participant => participant !== getUserIdFromCookie());
            chatRecipientsUsername = [];
            chatRecipientsId.forEach(id => chatRecipientsUsername.push(getUsername(id)));

            if (chatRecipientsUsername.length >= 1) {
                chatTitle = chatRecipientsUsername.join(', ');
            } else {
                createNotification("Client Error!", "Seems you using old client. Please clear browser cache and restart page!", 'danger', notification);
                chatTitle = chatId;
            }

            chatTitleList[i] = chatTitle;
            createChatInChatList(chatId, chatTitle, chatLastMessageTime, chatLastMessage);
        } else {
            updateChatInChatList(chatId, chatLastMessage, chatLastMessageTime);
        }
    }
}

/**
 * The connect() method establishes a connection to /ws,
 * where it expects connections from our server,
 * and also determines the callback function onConnected
 * that will be called upon successful connection,
 * and the onError called if an error occurs while connecting to the server.
 */
function connect() {
    var socket = new SockJS('/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function () {

        // subscribe on new chats
        stompClient.subscribe('/topic-chat/' + getUserIdFromCookie(), function (msg) {
            onChatCreated(msg);
        }, {id: 'user-' + getUserIdFromCookie()});

        stompClient.subscribe('/topic-message/' + getUserIdFromCookie(), function (msg) {
            onMessageDeleted(msg);
        }, {id: 'user-messages-' + getUserIdFromCookie()});

        // subscribe on each chat of current user
        if (chatIdList.length !== 0) {
            for (var i = 0; i < chatIdList.length; i += 1) {
                stompClient.subscribe('/chat/messages/' + chatIdList[i],
                    function (msg) {
                        onMessageReceived(msg);
                    }, {id: 'chat-' + chatIdList[i]}
                );
            }
        }
    }, function () {
        $('.main-section').fadeOut(500);
        disconnect();
        var t = setInterval(function () {
            isServerWorking(function (found) {
                if (found) {
                    createNotification("Connection is restored", "You are connected", 'success', notification);
                    clearInterval(t);
                    reconnected = true;
                    initIndex();
                    $('.main-section').fadeIn(500);
                } else {
                    createNotification("Reconnecting...", "Lost connection to the server. The chat page will automatically appear as soon as the connection is restored", 'warning', notification);
                }
            });
        }, 4500);
    });
}

function onChatCreated(msg) {
    const chat = JSON.parse(msg.body);
    const newChatId = chat.id;

    // if you created
    if (chatIdList.indexOf(newChatId) > -1)
        return;

    var chatTitle;
    var chatRecipientsId = getParticipantsByChatId(newChatId).filter(participant => participant !== getUserIdFromCookie());
    var chatRecipientsUsername = [];
    chatRecipientsId.forEach(id => chatRecipientsUsername.push(getUsername(id)));

    if (chatRecipientsUsername.length >= 1) {
        chatTitle = chatRecipientsUsername.join(', ');
    } else {
        chatTitle = newChatId;
    }

    chatIdList.push(newChatId);
    chatTitleList.push(chatTitle);
    chatMessagesList[chatIdList.indexOf(newChatId)] = [];

    createChatInChatList(newChatId, chatTitle, '', '');

    // subscribing to messages of new chat
    stompClient.subscribe('/chat/messages/' + newChatId,
        function (msg) {
            onMessageReceived(msg);
        }, {id: 'chat-' + newChatId}
    );
}

/**
 * The onMessageReceived(msg) method called when message comes from
 * known chat (which we subscribed for).
 * @param msg Message (JSON-like) which contains senderId, content, timeCreated and chatId.
 */
function onMessageReceived(msg) {
    const message = JSON.parse(msg.body);
    const messageTime = new Date(message.timeCreated).toLocaleTimeString('en-US', {
        hour12: false,
        hour: "numeric",
        minute: "numeric"
    });

    chatMessagesList[chatIdList.indexOf(message.chatId)].push(message);

    if (selectedChatId === message.chatId) {
        // show this message
        createMessageElement(message);
        updateChatHeaderRight(selectedChatTitle, message.content);
    }

    // move chat with new message to the top of chat list
    moveChatToTop(message.chatId);
    updateChatInChatList(message.chatId, message.content, messageTime);
}

/**
 * The sendMessage() creates JSON-like object
 * and send it via stompClient.
 */
function sendMessage(image = undefined) {
    const message = {
        senderId: getUserIdFromCookie(),
        content: image ? image : getMessageFieldValue(),
        timeCreated: new Date(),
        chatId: selectedChatId,
    };

    stompClient.send("/app/chat", {}, JSON.stringify(message));
}

function disconnect() {
    if (stompClient !== null) {
        // unsubscribe from all chat of currentUser
        for (let i = 0; i < chatIdList.length; i++) {
            stompClient.unsubscribe('chat-' + chatIdList[i]);
        }

        // unsubscribe from notifications (incoming new-chats)
        stompClient.unsubscribe('user-' + getUserIdFromCookie());
        stompClient.unsubscribe('user-messages-' + getUserIdFromCookie());
        stompClient.disconnect();
    }
}

function isServerWorking(callback) {
    var timer = setTimeout(function () {
        callback(false);
    }, 3500)

    var img = document.createElement("img");
    img.onload = function () {
        clearTimeout(timer);
        callback(true);
    }

    img.onerror = function () {
        clearTimeout(timer);
        callback(false);
    }

    img.src = URL + "/zero_element.svg?t=" + (+new Date);
}

function createMessageElement(message) {
    const messageTime = new Date(message.timeCreated).toLocaleTimeString('en-US', {
        hour12: false,
        hour: "numeric",
        minute: "numeric"
    });
    const side = message.senderId === getUserIdFromCookie() ? "right" : "left";
    const liClass = side === "left" ? '<li class="msg-left" id="' + message.id + '">\n' : '<li class="msg-right" id="' + message.id + '">\n';

    $('#listOfCurrentChatMessages').append(liClass +
        '\t\t\t\t\t\t\t<div class="msg-left-sub">\n' +
        '\t\t\t\t\t\t\t\t<img src="/awesome_cat.png" alt="awesome-cat">\n' +
        '\t\t\t\t\t\t\t\t<div class="msg-desc">\n' +
        '\t\t\t\t\t\t\t\t<div class="sender">' + getUsername(message.senderId) + '</div>\n' +
        '\t\t\t\t\t\t\t\t<div class="content">' + message.content + '</div>\n' +
        '\t\t\t\t\t\t\t\t</div>\n' +
        '\t\t\t\t\t\t\t\t<small>' + messageTime + '</small>\n' +
        '\t\t\t\t\t\t\t</div>\n' +
        '\t\t\t\t\t\t</li>');

    // if message is image -> show image
    if (message.content.startsWith("https://ucarecdn.com/")) {
        // if content is image
        let image = new Image();
        image.src = message.content;
        $("#listOfCurrentChatMessages li:last").find('.msg-desc .content').html(image);
    }
}

function formatLongTitleInChatElement(title) {
    return title.length > 16 ? title.substr(0, 15) + "..." : title;
}

function formatLongMessageInChatElement(message) {
    return message.length > 18 ? message.substr(0, 17) + "..." : message;
}

function getUsername(id) {
    var username;

    if (userIdList !== undefined) {
        const idIndex = userIdList.indexOf(id);

        if (idIndex > -1) {
            username = userUsernameList[idIndex];
        } else {
            username = getUsernameFromMongo(id);
            userIdList.push(id);
            userUsernameList.push(username);
        }
    } else {
        username = getUsernameFromMongo(id);
    }

    return username;
}

function getUserId(username) {
    var userId;

    if (userUsernameList !== undefined) {
        const usernameIndex = userUsernameList.indexOf(username);

        if (usernameIndex > -1) {
            userId = userIdList[usernameIndex];
        } else {
            userId = getUserIdFromMongo(username);
            userIdList.push(userId);
            userUsernameList.push(username);
        }
    } else {
        userId = getUserIdFromMongo(username);
    }

    return userId;
}

function getChatMessages(id) {
    const index = chatIdList.indexOf(id);

    if (index > -1) {
        if (chatMessagesList[index] === undefined) {
            chatMessagesList[index] = [];
            const messages = getChatMessagesFromMongo(id);

            for (let i = 0; i < messages.length; i += 1) {
                let rawCurrentMessage = JSON.parse(JSON.stringify(messages[i]));
                let message = {
                    id: rawCurrentMessage.id,
                    senderId: rawCurrentMessage.senderId,
                    content: rawCurrentMessage.content,
                    timeCreated: rawCurrentMessage.timeCreated,
                    chatId: rawCurrentMessage.chatId,
                };

                chatMessagesList[index].push(message);
            }
        }

        return chatMessagesList[index];
    }

    return [];
}

function getLastMessageContent(id) {

    const messagesList = getChatMessages(id);
    let lastMessageContent = '';

    if (messagesList.length > 0) {
        lastMessageContent = messagesList[messagesList.length - 1].content;
    }

    return lastMessageContent;
}

function getLastMessageTime(id) {

    var messagesList = getChatMessages(id);
    var lastMessageTime = '';

    if (messagesList.length > 0) {
        lastMessageTime = new Date(messagesList[messagesList.length - 1].timeCreated)
            .toLocaleTimeString('en-US', {
                hour12: false,
                hour: "numeric",
                minute: "numeric"
            });
    }

    return lastMessageTime;
}

function getMessageFieldValue() {
    return $('#sendMessageField').val();
}

function clearMessageField() {
    $('#sendMessageField').val("");
}

function getSearchFieldValue() {
    return $('#searchField').val();
}

function clearSearchField() {
    $('#searchField').val("");
}

function moveChatToTop(id) {
    if (id) {
        let list = $("#listOfChats");

        list.prepend(list.find("#" + id));
    }
}

function createChatInChatList(id, title, time, message) {
    const substrChatTitle = formatLongTitleInChatElement(title);
    const substrChatLastMessage = message.startsWith("https://ucarecdn.com/") ? 'Picture' : formatLongMessageInChatElement(message);

    $('#listOfChats').prepend('<li id="' + id + '" title="' + title + '">\n' +
        '                    <div class="chatList">\n' +
        '                        <div class="img">\n' +
        '                            <img src="/awesome_cat.png" alt="awesome-cat">\n' +
        '                        </div>\n' +
        '                        <div class="desc">\n' +
        '                            <small class="time">' + time + '</small>\n' +
        '                            <h5>' + substrChatTitle + '</h5>\n' +
        '                            <small>' + substrChatLastMessage + '</small>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                </li>');
}

function updateChatInChatList(id, body, time) {
    if (id && body && time) {
        let list = $("#listOfChats");

        list.find("#" + id + ' .desc .time').html(time);
        list.find("#" + id + ' .desc small').next().next().html(body.startsWith("https://ucarecdn.com/") ? 'Picture' : formatLongMessageInChatElement(body));
    }
}

function updateChatHeaderRight(chatTitle, message) {
    if (message.startsWith("https://ucarecdn.com/")) {
        $('.headRight-sub').html('\n' +
            '                <h3>' + chatTitle + '</h3>\n' +
            '                <small>Picture</small>\n' +
            '            ');
    } else {
        $('.headRight-sub').html('\n' +
            '                <h3>' + chatTitle + '</h3>\n' +
            '                <small>' + formatLongMessageInChatElement(message) + '</small>\n' +
            '            ');
    }
}

function makeChatActive(id, title = undefined) {
    if (id) {
        const list = $("#listOfChats");

        if (isCreatingGroupChatMode) {
            list.find("#" + id)
                .addClass('active');
        } else {
            if (title) {
                selectedChatId = id;
                selectedChatTitle = title;
                const lastMessage = getLastMessageContent(id);
                updateChatHeaderRight(title, lastMessage);
                $('#listOfCurrentChatMessages').empty();
                clearMessageField();
                $(".right-section").show();
                $("#sendMessageField").focus();
                $('#clearMessagesButton').hide();
            }

            list.find("#" + id)
                .addClass('active')
                .siblings()
                .removeClass('active');
        }
    }
}

function makeChatNonActive(id = undefined) {
    $(".right-section").hide();
    updateChatHeaderRight('', '');
    selectedMessagesIdList = [];

    if (id) {
        if (!isCreatingGroupChatMode) {
            selectedChatId = undefined;
            selectedChatTitle = undefined;
            clearMessageField();
        }

        $("#listOfChats").find("#" + id)
            .removeClass('active');
    } else {
        selectedChatId = undefined;
        selectedChatTitle = undefined;
        $("#listOfChats li").removeClass('active');
    }
}

function disableCreatingGroupChatMode() {
    const searchField = $('#searchField');
    isCreatingGroupChatMode = false;
    groupChatRecipientsList = [];
    selectedChatsList = [];
    makeChatNonActive();

    // show buttons
    $('#exitButton').show();
    searchField.show();
    searchField.css({
        'width': '73%',
        'border-radius': '10px',
        'border': '1px solid #E6E6E6',
        'padding': '7px',
    });

    // hide 'create chat' text
    $('#headTitle').hide();
}

function uploadImage() {
    let photo = document.getElementById("image-file").files[0];
    var reader = new FileReader();

    reader.onloadend = function () {
        // original base64 image
        const b64data = reader.result;

        // if not an image -> warning and stop
        if (!b64data.startsWith("data:image/")) {
            createNotification("Not an image!", "Upload supports only images.", 'warning', notification);
            $("#image-file").val('');
            return;
        }

        // convert dataURI to a File object
        function dataURLtoFile(dataurl, filename) {
            let arr = dataurl.split(','),
                mime = arr[0].match(/:(.*?);/)[1],
                bstr = atob(arr[1]),
                n = bstr.length,
                u8arr = new Uint8Array(n);
            while (n--) {
                u8arr[n] = bstr.charCodeAt(n);
            }
            return new File([u8arr], filename, {type: mime});
        }

        // convert a base64 image to a File object
        const fileToUpload = dataURLtoFile(b64data, "image");

        // upload the file to Uploadcare
        let upload = uploadcare.fileFrom("object", fileToUpload);

        upload.done(fileInfo => {
            sendMessage(fileInfo.cdnUrl);
        });

        // clear file
        $("#image-file").val('');
    }
    reader.readAsDataURL(photo);
}

function makeMessageActive(id) {
    if (id) {
        const list = $("#listOfCurrentChatMessages");

        list.find("#" + id).addClass('active');
        selectedMessagesIdList.push(id);
        $('#clearMessagesButton').show();

        console.log("message " + id + " became active");
    }
}

function makeMessageNonActive(id = undefined) {
    if (id) {
        $("#listOfCurrentChatMessages").find("#" + id)
            .removeClass('active');

        const index = selectedMessagesIdList.indexOf(id);
        if (index > -1)
            selectedMessagesIdList.splice(index, 1);

        if (selectedMessagesIdList.length === 0)
            $('#clearMessagesButton').hide();
    } else {
        $("#listOfCurrentChatMessages li").removeClass('active');
        selectedMessagesIdList = [];
        $('#clearMessagesButton').hide();
    }
}

function onMessageDeleted(msg) {
    msg = JSON.parse(msg.body);
    const chatIndex = chatIdList.indexOf(msg[0].chatId);

    for (var i = 0; i < msg.length; i += 1) {
        for (var j = 0; j < chatMessagesList[chatIndex].length; j += 1) {
            if (chatMessagesList[chatIndex][j].id === msg[i].id) {
                console.log("onMessageDeleted: removed message " + msg[i].id);
                chatMessagesList[chatIndex].splice(j, 1);
                break;
            }
        }
    }

    if (selectedChatId === msg[0].chatId) {
        for (var i = 0; i < msg.length; i += 1) {
            $('#' + msg[i].id).remove();
        }
        updateChatHeaderRight(selectedChatTitle, getLastMessageContent(selectedChatId));
    }
    updateChatInChatList(msg[0].chatId, getLastMessageContent(msg[0].chatId), getLastMessageTime(msg[0].chatId));
}