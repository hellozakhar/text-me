package com.hellozakhar.textme.repository;

import com.hellozakhar.textme.model.Participant;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ParticipantRepository extends MongoRepository<Participant, String> {

    List<Participant> findByUserId(String userId);

    List<Participant> findByChatId(String chatId);
}