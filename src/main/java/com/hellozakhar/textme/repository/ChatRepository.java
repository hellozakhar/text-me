package com.hellozakhar.textme.repository;

import com.hellozakhar.textme.model.Chat;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ChatRepository extends MongoRepository<Chat, String> {
}