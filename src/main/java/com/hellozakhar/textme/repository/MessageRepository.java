package com.hellozakhar.textme.repository;

import com.hellozakhar.textme.model.Message;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MessageRepository extends MongoRepository<Message, String> {

    List<Message> findByChatId(String chatId);
}