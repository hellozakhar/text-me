package com.hellozakhar.textme.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document
public class Participant {

    @Id
    private String id;

    @Field("chat_id")
    private String chatId;

    @Field("user_id")
    private String userId;
}