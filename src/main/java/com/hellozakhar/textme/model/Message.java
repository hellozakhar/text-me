package com.hellozakhar.textme.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document
public class Message {

    @Id
    private String id;

    @Field("sender_id")
    private String senderId;

    private String content;

    @Field("time_created")
    private Date timeCreated;

    @Field("chat_id")
    private String chatId;
}
