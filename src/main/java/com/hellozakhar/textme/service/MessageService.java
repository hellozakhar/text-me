package com.hellozakhar.textme.service;

import com.hellozakhar.textme.model.Message;
import com.hellozakhar.textme.repository.MessageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@Slf4j
public class MessageService {

    private final MessageRepository repository;

    @Autowired
    public MessageService(MessageRepository repository) {
        this.repository = repository;
    }

    /**
     * Saves Message object to mongo and return this object.
     *
     * @param message Message.
     * @return a message object.
     */
    public Message save(Message message) {
        repository.save(message);
        return message;
    }

    /**
     * Returns all messages of chat by provided chatId.
     *
     * @param chatId id of chat.
     * @return a list of chat messages.
     */
    public List<Message> findChatMessages(String chatId) {
        List<Message> messages = repository.findByChatId(chatId);
        if (messages.isEmpty()) {
            log.info("findChatMessages(chatId): No messages for chat {}.", chatId);
            return Collections.emptyList();
        }

        return messages;
    }

    public void deleteMessages(List<Message> messages) {
        for (Message message : messages) {
            log.info("deleteMessages: Message {} was deleted.", message.getId());
            repository.deleteById(message.getId());
        }
    }
}