package com.hellozakhar.textme.service;

import com.hellozakhar.textme.model.Participant;
import com.hellozakhar.textme.repository.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ParticipantService {

    private final ParticipantRepository repository;

    @Autowired
    public ParticipantService(ParticipantRepository repository) {
        this.repository = repository;
    }

    /**
     * Returns List of String objects, contains of user chat ids.
     *
     * @param userId id of user
     * @return list of chatIds
     */
    public List<String> findChatsByParticipantId(String userId) {
        List<Participant> participant = repository.findByUserId(userId);
        List<String> chatList = new ArrayList<>();

        participant.forEach(p -> chatList.add(p.getChatId()));

        return chatList;
    }

    /**
     * Returns List of String objects, contains of chat member ids.
     *
     * @param chatId id of chat
     * @return list of userIds
     */
    public List<String> findParticipantsByChatId(String chatId) {
        List<Participant> participant = repository.findByChatId(chatId);
        List<String> userList = new ArrayList<>();

        participant.forEach(p -> userList.add(p.getUserId()));

        return userList;
    }
}