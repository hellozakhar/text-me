package com.hellozakhar.textme.service;

import com.hellozakhar.textme.model.Chat;
import com.hellozakhar.textme.model.Participant;
import com.hellozakhar.textme.repository.ChatRepository;
import com.hellozakhar.textme.repository.ParticipantRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class ChatService {

    private final ChatRepository chatRepository;
    private final ParticipantRepository participantRepository;

    @Autowired
    public ChatService(ChatRepository chatRepository, ParticipantRepository participantRepository) {
        this.chatRepository = chatRepository;
        this.participantRepository = participantRepository;
    }

    /**
     * Returns chatId of chat with provided list of chat member id.
     *
     * @param userId list of userid.
     * @return Optional<Chat> chat if chat with participants with provided userIds,
     * otherwise returns Optional.empty().
     */
    public Optional<Chat> getChatId(List<String> userId) {
        Optional<Chat> chat = Optional.empty();

        // if userIds not enough then return Optional.empty()
        if (userId.size() < 2)
            return Optional.empty();

        // initialize empty collection
        Collection<String> intersect = CollectionUtils.emptyCollection();

        for (int i = 0; i < userId.size(); i++) {
            // get list participant objects which means current user as chats member
            List<Participant> userAsParticipant = participantRepository.findByUserId(userId.get(i));

            List<String> currentChatsList = new ArrayList<>();
            // creating chat list based on chat membership of current user
            userAsParticipant.forEach(participant -> currentChatsList.add(participant.getChatId()));

            if (i == 0) {
                // first time, intersect is list of chatIds of first user in list
                intersect = currentChatsList;
            } else {
                // find intersection between chatIds of previous user and current user
                // simple example:
                // first user in chats '0001', '0002',
                // and second user in chats '0002', '0003',
                // intersect will be '0002'.
                intersect = CollectionUtils.intersection(intersect, currentChatsList);
            }
        }

        // if intersect not empty (which means chat with these users exists)
        if (!intersect.isEmpty()) {
            String chatId;
            Iterator<String> chats = intersect.iterator();

            while (chats.hasNext()) {
                // get element from collection "intersect"
                chatId = chats.next();

                if (participantRepository.findByChatId(chatId).size() == userId.size()) {
                    // find chat by chatId
                    chat = chatRepository.findById(chatId);
                    chat.ifPresent(value -> log.info("getChatId(userId): Chat {} for users {} already exists.", value.getId(), userId));
                    break;
                }
            }
        }

        return chat;
    }

    /**
     * Creates chat with provided userIds and returns chat id.
     *
     * @param userId list of userIds.
     * @return chatId if chat created, or "" if not.
     */
    public String createChat(List<String> userId) {
        Optional<Chat> chat = Optional.of(Chat
                .builder()
                .build());

        // save chat to mongo
        chatRepository.save(chat.get());

        log.info("getChatId(userId): Chat {} for users {} was created.", chat.get().getId(), userId);

        // for each userId create Participant object as member of chat
        // and save it to mongo
        for (String currentUserId : userId) {
            Participant participant = Participant
                    .builder()
                    .userId(currentUserId)
                    .chatId(chat.get().getId())
                    .build();

            participantRepository.save(participant);
            log.info("getChatId(userId): Participant (userId={}) added to chat {}.", currentUserId, chat.get().getId());
        }

        return chat.get().getId();
    }

    /**
     * Returns all chats from mongo.
     *
     * @return a List of Chat objects.
     */
    public List<Chat> getAllChats() {
        List<Chat> chats = chatRepository.findAll();

        return chats;
    }
}