package com.hellozakhar.textme.service;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.hellozakhar.textme.model.User;
import com.hellozakhar.textme.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Tries to login user by provided username and password.
     * Password encodes and compares with password from mongo.
     *
     * @param username username of user.
     * @param password password of user.
     * @return true if credentials(username & password) are correct,
     * otherwise, returns false.
     */
    public boolean loginUser(String username, String password) {
        User user = findByUsername(username).orElse(new User());

        if (user.getUsername() != null)
            return BCrypt
                    .verifyer()
                    .verify(password.toCharArray(), user.getPassword())
                    .verified;
        else
            return false;
    }

    /**
     * Tries to register user by provided User-object.
     * If username is unique, encodes password and saves user to mongo.
     *
     * @param user provided User object, contains username & password.
     * @return true, if user with username already not exists in mongo,
     * otherwise, returns false.
     */
    public boolean registerUser(User user) {
        log.info("registering user {}", user.getUsername());

        if (userRepository.existsByUsername(user.getUsername())) {
            log.warn("username {} already exists", user.getUsername());
            return false;
        }

        user.setPassword(BCrypt.withDefaults().hashToString(12, user.getPassword().toCharArray()));
        userRepository.save(user);
        return true;
    }

    /**
     * Returns List of User objects from mongo and prints log.
     *
     * @return a List of User objects.
     */
    public List<User> findAll() {
        log.info("retrieving all users");
        return userRepository.findAll();
    }

    /**
     * Returns User object by provided username of user.
     *
     * @param username is name of user.
     * @return a User object, if user with this username present,
     * otherwise returns Optional.empty().
     */
    public Optional<User> findByUsername(String username) {
        log.info("retrieving user {}", username);
        return userRepository.findByUsername(username);
    }

    /**
     * Returns User object by provided id of user.
     *
     * @param id user id.
     * @return a User object, if user with this id present,
     * otherwise returns Optional.empty().
     */
    public Optional<User> findById(String id) {
        log.info("retrieving user {}", id);
        return userRepository.findById(id);
    }

    /**
     * Returns an Optional<User> by provided username of user.
     *
     * @param username is name of user.
     * @return Optional<User> which present if user with username exists,
     * otherwise returns Optional.empty()
     */
    public Optional<User> getUserByUsername(String username) {
        Optional<User> user = findByUsername(username);

        if (user.isPresent()) {
            log.info("get userId: {} of user: {}", user.get().getId(), user.get().getUsername());
            return user;
        } else {
            log.info("user with username: {} is not exist", username);
            return Optional.empty();
        }
    }

    /**
     * Returns an Optional<User> by provided id of user.
     *
     * @param id is userId.
     * @return Optional<User> which present if user with id exists,
     * otherwise returns Optional.empty()
     */
    public Optional<User> getUserByUserId(String id) {
        Optional<User> user = findById(id);

        if (user.isPresent()) {
            log.info("get username: {} of user with id: {}", user.get().getUsername(), user.get().getId());
            return user;
        } else {
            log.info("user with id: {} is not exist", id);
            return Optional.empty();
        }
    }
}