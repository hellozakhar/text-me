package com.hellozakhar.textme.controller;

import com.hellozakhar.textme.model.User;
import com.hellozakhar.textme.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@Slf4j
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {

        this.userService = userService;

        userService.registerUser(User
                .builder()
                .username("admin")
                .password("123456")
                .build());
    }

    /**
     * Method which calls on HTTP GET-request '/home' and returns "home"
     *
     * @return "home"
     */
    @GetMapping("/home")
    public String getHome() {

        return "home";
    }

    /**
     * Method which calls on HTTP GET-request '/signIn' and returns "signIn"
     *
     * @return "signIn"
     */
    @GetMapping("/signIn")
    public String getSignIn() {

        return "signIn";
    }

    /**
     * Method which calls on HTTP GET-request '/signUp' and returns "signUp"
     *
     * @return "signUp"
     */
    @GetMapping("/signUp")
    public String getSignUp() {

        return "signUp";
    }

    /**
     * Method determines 'signIn user' behaviour, which calls on HTTP
     * POST-request and returns ResponseEntity object with status-code,
     * based on success of signIn user.
     *
     * @param credentials is a User object, containing username and password.
     * @return a ResponseEntity object with status-code OK if signIn of user was successful
     * otherwise, returns ResponseEntity object with status-code UNAUTHORIZED.
     */
    @PostMapping("/signIn")
    @ResponseBody
    public ResponseEntity<?> signIn(@Valid @RequestBody User credentials) {

        log.info("signIn user {}", credentials.getUsername());

        boolean success = userService.loginUser(credentials.getUsername(),
                credentials.getPassword());

        if (success) {
            log.info("successfully signIn user {}", credentials.getUsername());
        } else {
            log.info("failed signIn user {}", credentials.getUsername());
        }

        return success ?
                new ResponseEntity<>("Successful signIn!",
                        HttpStatus.OK) :
                new ResponseEntity<>("Failed signIn! Reason: Login or password are incorrect.",
                        HttpStatus.UNAUTHORIZED);
    }

    /**
     * Method determines 'signUp user' behaviour, which calls on HTTP
     * POST-request and returns ResponseEntity object with status-code,
     * based on success of signUp user.
     *
     * @param credentials is a User object, containing username and password.
     * @return a ResponseEntity object with status-code OK if signUp of user was successful
     * otherwise, returns ResponseEntity object with status-code UNAUTHORIZED.
     */
    @PostMapping("/signUp")
    @ResponseBody
    public ResponseEntity<?> signUp(@Valid @RequestBody User credentials) {

        log.info("creating user {}", credentials.getUsername());

        User user = User
                .builder()
                .username(credentials.getUsername())
                .password(credentials.getPassword())
                .build();

        boolean success = userService.registerUser(user);

        if (success) {
            log.info("successfully registered user {}", user.getUsername());
        } else {
            log.info("failed register user {}", user.getUsername());
        }

        return success ?
                new ResponseEntity<>("Successful signUp!",
                        HttpStatus.OK) :
                new ResponseEntity<>("Failed signUp!",
                        HttpStatus.UNAUTHORIZED);
    }

    /**
     * Method determines 'get User object by username' behaviour, which calls on HTTP
     * GET-request and returns ResponseEntity object with status-code 'OK',
     * and body, contains User object.
     *
     * @param username is a name of user.
     * @return a ResponseEntity object with status-code OK and body,
     * contains User object.
     */
    @GetMapping("/user")
    public ResponseEntity<?> findUserByUsername(@RequestParam String username) {
        return ResponseEntity
                .ok(userService.getUserByUsername(username));
    }

    /**
     * Method determines 'get User object by id of user' behaviour, which calls on HTTP
     * GET-request and returns ResponseEntity object with status-code 'OK',
     * and body, contains User object.
     *
     * @param id is a user's id.
     * @return a ResponseEntity object with status-code OK and body,
     * contains User object.
     */
    @GetMapping("/user/{id}")
    public ResponseEntity<?> findUserById(@PathVariable String id) {
        return ResponseEntity
                .ok(userService.getUserByUserId(id));
    }
}