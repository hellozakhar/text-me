package com.hellozakhar.textme.controller;

import com.hellozakhar.textme.model.Chat;
import com.hellozakhar.textme.model.Message;
import com.hellozakhar.textme.model.User;
import com.hellozakhar.textme.service.ChatService;
import com.hellozakhar.textme.service.MessageService;
import com.hellozakhar.textme.service.ParticipantService;
import com.hellozakhar.textme.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
public class ChatController {

    private final SimpMessagingTemplate messagingTemplate;
    private final ChatService chatService;
    private final MessageService messageService;
    private final UserService userService;
    private final ParticipantService participantService;

    @Autowired
    public ChatController(SimpMessagingTemplate messagingTemplate,
                          ChatService chatService,
                          MessageService messageService,
                          UserService userService,
                          ParticipantService participantService) {

        this.messagingTemplate = messagingTemplate;
        this.chatService = chatService;
        this.messageService = messageService;
        this.userService = userService;
        this.participantService = participantService;
    }

    /**
     * Process (save to mongo and send) provided message object.
     *
     * @param message Message object.
     */
    @MessageMapping("/chat")
    public void processMessage(Message message) {
        Message saved = messageService.save(message);
        messagingTemplate.convertAndSend("/chat/messages/" + message.getChatId(), saved);
    }

    /**
     * Sends created chat object to participants.
     *
     * @param chat Chat object.
     */
    @MessageMapping("/topic-chat")
    public void sendNotificationToCreateChat(Chat chat) {
        List<String> participants = participantService.findParticipantsByChatId(chat.getId());

        for (String user : participants) {
            messagingTemplate.convertAndSend("/topic-chat/" + user, chat);
        }

        Optional<User> admin = userService.getUserByUsername("admin");
        if (admin.isPresent())
            messagingTemplate.convertAndSend("/topic-chat/" + admin.get().getId(), chat);
    }

    /**
     * Sends messages to be deleted.
     *
     * @param messages is a List of Message objects to be deleted.
     */
    @MessageMapping("/topic-message")
    public void sendNotificationToDeleteMessages(List<Message> messages) {
        if (messages.size() == 0)
            return;

        List<String> participants = participantService.findParticipantsByChatId(messages.get(0).getChatId());

        for (String user : participants) {
            messagingTemplate.convertAndSend("/topic-message/" + user, messages);
        }

        Optional<User> admin = userService.getUserByUsername("admin");
        if (admin.isPresent())
            messagingTemplate.convertAndSend("/topic-message/" + admin.get().getId(), messages);
    }

    /**
     * Returns id of chat, which calls on HTTP
     * GET-request and returns ResponseEntity object with status-code 'OK',
     * and body, contains chat id.
     *
     * @param userId is an id of user.
     * @return a ResponseEntity object with status-code OK and body,
     * contains chat id.
     */
    @GetMapping("/chat")
    public ResponseEntity<?> getChatId(@RequestParam List<String> userId) {
        return ResponseEntity
                .ok(chatService.getChatId(userId));
    }

    /**
     * Returns participants of chat, which calls on HTTP
     * GET-request and returns ResponseEntity object with status-code 'OK',
     * and body, contains list of participants.
     *
     * @param id is an id of chat.
     * @return a ResponseEntity object with status-code OK and body,
     * contains list of participants.
     */
    @GetMapping("/chat/{id}")
    public ResponseEntity<?> findChatUsers(@PathVariable String id) {
        return ResponseEntity
                .ok(participantService.findParticipantsByChatId(id));
    }

    /**
     * Creates chat and return it's id, which calls on HTTP
     * POST-request and returns ResponseEntity object with status-code 'OK',
     * and body, contains id of chat.
     *
     * @param userId is a List of user ids.
     * @return a ResponseEntity object with status-code OK and body,
     * contains id of chat.
     */
    @PostMapping(value = "/chat/create")
    @ResponseBody
    public ResponseEntity<?> createChatAndGetId(@Valid @RequestBody List<String> userId) {
        return ResponseEntity.
                ok(chatService.createChat(userId));
    }

    /**
     * Returns all chats from mongo repository, which calls on HTTP
     * GET-request and returns ResponseEntity object with status-code 'OK',
     * and body, contains list of chats.
     *
     * @return a ResponseEntity object with status-code OK and body,
     * contains List of Chat objects.
     */
    @GetMapping("/chat/all")
    public ResponseEntity<?> findChats() {
        return ResponseEntity
                .ok(chatService.getAllChats());
    }

    /**
     * Returns user chats, which calls on HTTP
     * GET-request and returns ResponseEntity object with status-code 'OK',
     * and body, contains list of user chat ids.
     *
     * @param id is an id of user.
     * @return a ResponseEntity object with status-code OK and body,
     * contains list of user chat ids.
     */
    @GetMapping("/chat/user/{id}")
    public ResponseEntity<?> findUserChats(@PathVariable String id) {
        return ResponseEntity
                .ok(participantService.findChatsByParticipantId(id));
    }

    /**
     * Returns list of chat messages, which calls on HTTP
     * GET-request and returns ResponseEntity object with status-code 'OK',
     * and body, contains chat messages list.
     *
     * @param chatId is an id of chat.
     * @return a ResponseEntity object with status-code OK and body,
     * contains list of chat messages.
     */
    @GetMapping("/message")
    public ResponseEntity<?> findChatMessages(@RequestParam String chatId) {
        return ResponseEntity
                .ok(messageService.findChatMessages(chatId));
    }

    /**
     * Deletes provided messages in mongodb, which calls on HTTP
     * POST-request and returns ResponseEntity object with status-code 'OK'.
     *
     * @param messages is a List of Message objects to be deleted.
     * @return a ResponseEntity object with status-code OK.
     */
    @PostMapping(value = "/message/delete")
    @ResponseBody
    public ResponseEntity<?> deleteMessages(@Valid @RequestBody List<Message> messages) {
        messageService.deleteMessages(messages);
        return ResponseEntity.ok().build();
    }
}