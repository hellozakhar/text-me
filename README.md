# Text Me Messenger

TextMe Messenger, or simply TextMe, is a service which allows users to send text messages.

[Try TextMe now!](http://textme-env.eba-a8896ru7.eu-west-2.elasticbeanstalk.com)

[Functional Design](https://gitlab.com/hellozakhar/text-me/-/blob/master/FUNCTIONAL-DESIGN.md)

[Local Run Project](https://gitlab.com/hellozakhar/text-me/-/blob/master/LOCAL-RUN-PROJECT.md)