# Text Me Messenger

Before running project, we need run MongoDB.

### Run database instance

If MongoDB not installed, follow these steps.

##### Install MongoDB (for Windows OS)
1. Download the installer *.msi (https://www.mongodb.com/try/download/community?tck=docs_server)

   1.1 In the Version dropdown, select the version of MongoDB to download

   1.2 In the Platform dropdown, select *your platform*

   1.3 In the Package dropdown, select msi

   1.4 Click Download

2. Run the MongoDB installer.
3. Follow the MongoDB Community Edition installation wizard.
4. Add MongoDB binaries to the System PATH.
   You can add

   ```C:\Program Files\MongoDB\Server\4.4\bin```

   to your System PATH and then omit the full path to the MongoDB binaries.

##### Install MongoDB (for MacOS)

Follow these steps to install MongoDB Community Edition using Homebrew’s brew package manager.

1. Tap the MongoDB Homebrew Tap to download the official Homebrew formula for MongoDB and the Database Tools, by running the following command in your macOS Terminal:

   ```brew tap mongodb/brew```

2. To install MongoDB, run the following command in your macOS Terminal application:

   ```brew install mongodb-community@4.4```

#### Run MongoDB as service

###### Start MongoDB Service (For Windows users)
To start/restart the MongoDB service, use the Services console:
1. From the Services console, locate the MongoDB service.
2. Right-click on the MongoDB service and click Start.

To begin using MongoDB, connect a mongo.exe shell to the running MongoDB instance.
To connect, open a Command Interpreter with Administrative privileges and run:

```"C:\Program Files\MongoDB\Server\4.4\bin\mongo.exe"```

###### Start MongoDB Service (For MacOS users)

To run MongoDB (i.e. the mongod process) as a macOS service, issue the following:

```brew services start mongodb-community@4.4```

To stop a mongod running as a macOS service, use the following command as needed:

```brew services stop mongodb-community@4.4```

If you need to restart a mongod, use the following command:

```brew services restart mongodb-community```


##### Check MongoDB commands

1. Open console (Terminal in MacOS)
2. Run command ```mongo```
3. If MongoDB Service is running, console will print message like ```MongoDB shell version v4.4.3``` and connect to MongoDB Server.

### Run project

Download project "text-me-messenger". Then open your Intellij IDEA.

##### Import Project

To import project, follow these steps:

1. File -> New -> Project from Existing Sources
2. Choose downloaded project
3. Press "Open" button.

##### Run Project

To run project, open in sidebar "Project".
Then open file "TextMeApplication" in project's folders.
To the left of the class there is a green triangle.
To start the project, click on it.

If you don't see green triangle, you should edit configuration.

If application run ends with error,
try to restart mongodb service and run application again.