# Text Me Messenger

## Требования к веб-мессенджеру

* работает в браузере
* возможность регистрации / авторизации
* после прохождения авторизации перебрасывает в чат
* по нажатию на чат в окне выводятся сообщения (переписка), а также от кого отправлено каждое сообщение
* возможность поиска пользователей (дословный)
* вывод списка чатов
* осуществить хранение логинов-паролей в бд сервера


## Collections

### Users

| Name         | Type                      |
| :----------- | ------------------------- |
| _id          | ObjectID                  |
| name         | String                    |
| password     | Password                  |
| isAdmin      | Boolean                   |



### Chats

| Name                                 | Type             |
| ------------------------------------ | ---------------- |
| _id                                  | ObjectID         |



### Participants

| Name                          | Type             |
| ----------------------------- | ---------------- |
| _id                           | ObjectID         |
| chat_id (FK->"Chats")         | ObjectID         |
| user_id (FK->"Users")         | ObjectID         |



### Message

| Name                    | Type               |
| ----------------------- | ------------------ |
| _id                     | ObjectID           |
| sender_id (FK->"Users") | ObjectID           |
| content                 | String             |
| time_created            | Timestamp / Date   |
| chat_id (FK->"Chats")   | ObjectID           |